package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BookControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_get_200_status_code_for_user_2() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("The book for user 2"));
    }

    @Test
    void should_get_200_status_code_for_user_23() throws Exception {
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("The book for user 23"));
    }

    @Test
    void should_get_failed_when_pathVariable_is_case_sensitive() throws Exception {
        mockMvc.perform(get("/api/users/2/BOOKS"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_verify_priority_of_pathVariable() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(content().string("It is not returned by Path Variable"));
    }

    @Test
    void should_verify_word_wildcards_when_words_exceeded() throws Exception {
        mockMvc.perform(get("/api/users/helloxyz"))
                .andExpect(status().is(404));
    }

    @Test
    void should_verify_word_wildcards_when_word_missing() throws Exception {
        mockMvc.perform(get("/api/users/hell"))
                .andExpect(status().is(404));
    }

    @Test
    void should_verify_section_wildcards() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything"))
                .andExpect(status().isOk());
    }

    @Test
    void should_verify_section_wildcards_when_it_between_URL() throws Exception {
        mockMvc.perform(get("/api/wildcards/before/between/after"))
                .andExpect(status().isOk());
    }

    @Test
    void should_verify_section_wildcards_when_between_url_not_exist() throws Exception{
        mockMvc.perform(get("/api/wildcards/before/after"))
                .andExpect(status().is(404));
    }

    @Test
    void should_verify_section_wildcards_when_it_in_prefix() throws Exception {
        mockMvc.perform(get("/api/wildcards/prefix/after"))
                .andExpect(status().isOk());
    }

    @Test
    void should_verify_multi_section_wildcards_when_it_between_URL() throws  Exception{
        mockMvc.perform(get("/api/multiWildcards/before/between/after"))
                .andExpect(status().isOk());
    }

    @Test
    void should_verify_regex_in_url() throws Exception {
        mockMvc.perform(get("/api/users/regex1234"))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_request_by_query_string() throws Exception{
        mockMvc.perform(get("/api/users/query?request=apple"))
                .andExpect(status().isOk())
                .andExpect(content().string("It is an apple"));
    }

    @Test
    void should_get_request_by_query_without_given_param() throws Exception{
        mockMvc.perform(get("/api/users/query"))
                .andExpect(status().isBadRequest());
    }

}
