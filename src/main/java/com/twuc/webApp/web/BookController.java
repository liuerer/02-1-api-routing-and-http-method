package com.twuc.webApp.web;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class BookController {

    @GetMapping("/users/{userId}/books")
    public String get(@PathVariable long userId){
        return "The book for user " + userId;
    }

    @GetMapping("/segments/good")
    public String get_segments_good(){
        return "It is not returned by Path Variable";
    }

    @GetMapping("/segments/{segmentName}")
    public String get_segments_good(@PathVariable String segmentName){
        return "It is returned by Path Variable";
    }

    @GetMapping("/users/hell?")
    public String get_hello(){
        return "Hello World!";
    }

    @GetMapping("/wildcards/*")
    public String get_anything(){
        return "Hello Anything!";
    }

    @GetMapping("/wildcards/before/*/after")
    public String get_between(){
        return "Between!";
    }

    @GetMapping("/wildcards/pre*/after")
    public String get_prefix(){
        return "Prefix!";
    }
    @GetMapping("/multiWildcards/**/after")
    public String get_multi_wildcards(){
        return "Multi_wildcards!";
    }

    @GetMapping("/users/regex{[0-9]+}")
    public String get_regex(){
        return "Regex!";
    }

    @GetMapping("/users/query")
    public String get_request(@RequestParam String request){
        return "It is an " + request;
    }

}
